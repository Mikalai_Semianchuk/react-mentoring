import React from "react";

import {Header} from "./Header/Header"
import {Body} from "./Body/Body"

export const App = () => {
    return (
        <>
            <Header/>
            <Body/>
        </>
    )
}