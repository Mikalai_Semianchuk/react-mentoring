import React from "react";
import PropTypes from "prop-types"

import "./ResultsFilter.scss"

export const ResultsFilter = ({text}) => {
    return <button className='resultsFilter'>{text}</button>
}

ResultsFilter.propTypes = {
    text: PropTypes.string.isRequired
}