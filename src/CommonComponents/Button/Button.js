import React from "react";

import "./Button.scss"

export const Button = ({height, width, text, color, background}) => {
    return <div className='button' style={{"height":height, "width":width, "color":color, "background":background}}>{text}</div>
}