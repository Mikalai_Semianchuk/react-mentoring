import imageOne from "./assets/images/image1.jpg"
import imageTwo from "./assets/images/image2.jpg"
import imageThree from "./assets/images/image3.jpg"
import imageFour from "./assets/images/image4.jpg"

export const movies = [
    {
        id: 1,
        title: 'Pirates of the Caribbean',
        releaseDate: 2001,
        gender: 'Documentary',
        image: imageOne,
    },
    {
        id: 2,
        title: 'Avatar',
        releaseDate: 2002,
        gender: 'Comedy',
        image: imageTwo
    },
    {
        id: 3,
        title: 'Rampage',
        releaseDate: 2003,
        gender: 'Horror',
        image: imageThree
    },
    {
        id: 4,
        title: 'X-men',
        releaseDate: 2004,
        gender: 'Crime',
        image: imageFour
    }
]
