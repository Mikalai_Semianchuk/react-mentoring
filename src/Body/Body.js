import React from "react";

import "./Body.scss"
import {ResultsFilter} from "../CommonComponents/ResultsFilter/ResultsFilter"
import {ResultsSort} from "./ResultsSort/ResultsSort"
import {ItemsContainer} from "./ItemsContainer/ItemsContainer"
import {ResultCount} from "./ResultCount/ResultCount"
import {ErrorBoundary} from "../CommonComponents/ErrorBoundary/ErrorBoundary"
import {movies} from "../Mocked"


export const Body = () => {
    return (
        <div className='body-wrapper'>
            <div className='body-filter-panel'>  
                    <div className='resultsFilter-wrapper'>
                        <ResultsFilter
                            text='All'
                        />
                        <ResultsFilter
                            text='Documentary'
                        />
                        <ResultsFilter
                            text='Comedy'
                        />
                        <ResultsFilter
                            text='Horror'
                        />
                        <ResultsFilter
                            text='Crime'
                        />
                    </div>
                    <ResultsSort/>
                </div>

            <ResultCount movies={movies.length} />
            <ErrorBoundary>
                <ItemsContainer movies={movies}/> 
            </ErrorBoundary>
               
        </div>
    )
}