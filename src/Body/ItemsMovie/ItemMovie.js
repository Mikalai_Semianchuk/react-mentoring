import React from 'react';
import PropTypes from "prop-types"

import './ItemMovie.scss';

export const ItemMovie = ({ id, title, releaseDate, gender, image }) => {
    return (
        <div className="itemMovie-wrapper" id={id}>
            <img className="itemMovie-image" src={image}/>
            <h1 className="itemMovie-title">{title}</h1>
            <p className="itemMovie-releaseDate"> {releaseDate} </p>
            <p className="itemMovie-gender"> {gender} </p>
        </div>)
}

ItemMovie.propTypes = {
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    releaseDate: PropTypes.number.isRequired,
    gender: PropTypes.string.isRequired
}