import React from 'react';

import { ItemMovie } from "../ItemsMovie/ItemMovie";
import './ItemsContainer.scss';


export const ItemsContainer = ({movies}) => {
        return (
            <div className="itemsContainer-wrapper">
                {movies.map(movie => (
                    <ItemMovie
                     title={movie.title}
                     releaseDate={movie.releaseDate}
                     gender={movie.gender}
                     image={movie.image}
                     key={movie.id} />
                ))
                }
            </div>
        );    
}
