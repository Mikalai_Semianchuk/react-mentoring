import React from 'react';

import "./ResultCount.scss";

export const ResultCount = ({movies}) => {
    return (
    <div className='resultCount'>{movies} movies found</div>
    )   
}