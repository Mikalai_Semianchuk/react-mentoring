import React from "react";

import "./ResultsSort.scss"

export const ResultsSort = () => {
    return (
        <div className='resultsSort'>
            <span className='dropDown-sortBy'>Sort By</span>
            <div class="dropdown">
                <button class="dropbtn">Dropdown</button>
                <div class="dropdown-content">
                    <a href="#">Release Date</a>
                    <a href="#">Film Name</a>
                </div>
            </div>
        </div>   
    )
}