import React from "react";
import "./Header.scss"

import { HeaderSearch } from "./HeaderSearch/HeaderSearch"
import { Button } from "../CommonComponents/Button/Button"

export const Header = () => {
    return (
        <>
            <div className="header-wrapper">
                <div>
                    <div className='header-top'>
                        <span className='header-netflix'>netflixroulette</span>
                        <Button className='header-addButton'
                            width='auto'
                            height='40px'
                            color='#f65261'
                            background='#555555'
                            text='+ Add Movie'
                        />
                    </div>

                    <div className='header-search-wrapper'>
                        <HeaderSearch />
                        <Button
                            width='120px'
                            height='40px'
                            color='white'
                            background='#f65261'
                            text='Search'
                        />
                    </div>
                </div>
            </div>
        </>
    )
}