import React from "react";
import "./HeaderSearch.scss"

export const HeaderSearch = () => {
    return <input placeholder="What do you want to watch?" className='HeaderSearch'></input>
}